package it.unibo.arkanoid.engine;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import it.unibo.arkanoid.controller.Command;

/**
 * The game engine.
 *
 */

public class GameLoop {

    private static final int MAXINPUT = 50;
    private static final int FPS = 20;
    private long period = FPS;
    private BlockingQueue<Command> commandQueue;

    public GameLoop() {
        this.commandQueue = new ArrayBlockingQueue<Command>(MAXINPUT);
    }

    public void inifiniteLoop() {

        long lastTime = System.currentTimeMillis();

        while (true) {
            final long current = System.currentTimeMillis();
            final int elapsed = (int) (current - lastTime);
            processInput();
            updateGame(elapsed);
            render();
            waitNextFrame(current);
            lastTime = current;
        }
    }

    protected void initialize() {

    }

    private void waitNextFrame(final long current) {
        long delta = System.currentTimeMillis() - current;
        if (delta < period) {
            try {
                Thread.sleep(period - delta);
            } catch (Exception ex) {
            }
        }
    }

    private void processInput() {
        Command command = commandQueue.poll();
        if (command != null) {
            command.execute(/* gameState */);
        }
    }

    private void updateGame(final int elapsed) {
        /* gameState.update(elapsed); */
    }

    private void render() {
        /* view.render(); */
    }

}

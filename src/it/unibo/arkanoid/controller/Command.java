package it.unibo.arkanoid.controller;
/**
 * 
 * Interface must be implemented from entity input.
 *
 */
public interface Command {

    void execute(/*GameState state*/);

}

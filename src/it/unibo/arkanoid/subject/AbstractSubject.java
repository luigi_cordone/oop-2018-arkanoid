package it.unibo.arkanoid.subject;

import it.unibo.arkanoid.utility.Position2D;
import javafx.geometry.*;


/**
 * Abstract class for for subject in the game's world.
 *
 */

public abstract class AbstractSubject extends BoundingBox {

    private Position2D position;
    private double width;
    private double height;

    /**
     * 
     * @param minX
     * @param minY
     * @param width
     * @param height
     */

    public AbstractSubject(final double minX, final double minY, final double width, final double height) {
        super(minX, minY, width, height);
        position = new Position2D(minX, minY);
    }

    private Position2D getPosition() {
        return this.position;
    }

    private Rectangle2D getBoundary() {
        return new Rectangle2D(position.getX(), position.getY(), this.width, this.height);
    }


}

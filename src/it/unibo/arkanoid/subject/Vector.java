package it.unibo.arkanoid.subject;

/**
 * Vector with x and y coordinate
 *
 */

public interface Vector {

    /**
     * Getter for the x coordinate.
     * @return the x coordinate
     */
     double getX();

    /**
     * Getter for the y coordinate.
     * @return the y coordinate
     */
     double getY();

    /**
     * Setter for the x coordinate.
     * @param x
     *         x coordinate
     */
    void setX(double x);

    /**
     * Setter for yhe y coordinate.
     * @param y
     *          y coordinate.
     */
    void setY(double y);

    /**
     * Set the position.
     * @param x
     *          x coordinate
     * @param y
     *          y coordinate
     */
    void setPosition(double x, double y);


}

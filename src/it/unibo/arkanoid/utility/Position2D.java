package it.unibo.arkanoid.utility;

import javafx.geometry.Point2D;

/**
 * Representation of a point in a two dimension with x and y coordinate.
 *
 */
public class Position2D extends Point2D {

    private double x;
    private double y;

    /**
     * 
     * @param x coordinate.
     * @param y coordinate.
     */
    public Position2D(final double x, final double y) {
        super(x, y);
    }

}

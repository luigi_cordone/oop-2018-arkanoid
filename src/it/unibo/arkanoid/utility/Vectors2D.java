package it.unibo.arkanoid.utility;

import math.geom2d.Vector2D;

/**
 * Representation of vector in the x and y coordinate.
 *
 */
public class Vectors2D extends Vector2D {

    private double x;
    private double y;

    /**
     * Constructor for Vector with x and y coordinate.
     * @param x coordinate.
     * @param y coordinate.
     */

    public Vectors2D(final double x, final double y) {
        this.x = x;
        this.y = y;
    }




}

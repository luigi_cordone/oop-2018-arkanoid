package it.unibo.arkanoid.main;

import javafx.application.Application;
import javafx.stage.Stage;

/**
 * The main class.
 *
 */

public class Main extends Application {

    /** Create folder if doesn't exit and launch application.
     * 
     * @param args
     *            argument
     */

    public static void main(final String[] args) {
        launch(args);
    }

    @Override
    public void start(final Stage stage) throws Exception {
        stage.setTitle("Arkanoid");
        stage.show();
    }

}
